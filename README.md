# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## 網站網址

https://105062171.gitlab.io/Assignment_02

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|O|
|Basic rules|20%|O|
|Jucify mechanisms|15%|O|
|Animations|10%|O|
|Particle Systems|10%|O|
|UI|5%|O|
|Sound effects|5%|O|
|Leaderboard|5%|N|

## Website Detail Description
game process:
   1. 點擊紅色start按鈕進入遊戲。
   2. 遊戲結束後，點擊畫面即可再次遊玩。
    
Basic rules:

   *  玩家(空白鍵發射)，敵人皆可發射子彈。
   *  玩家或敵人碰到就會往生 。
   *  地圖會往下方移動。
# Basic Components Description : 
1. Jucify mechanisms : 
    *  Level:
        當敵人越來越少的時候，敵人發射的子彈會越來越快，發射的間距也會變短。
    *  Skill:
        按"Z"可以發射超大雷射炮，殲滅路徑上敵人，一共可以發射三發。
2. Animations : 
    玩家的戰機有左,右跟禁止不動時的動畫。
3. Particle Systems : 
    *  當玩家的子彈打到敵人時，敵人的紫色彈藥會噴散出來。
    *  當敵人打到玩家時，玩家的pepega彈藥庫也會傾洩而出。
4. Sound effects :
    *  激昂的星戰背景音樂。
    *  玩家發射pepega時的音效。
    *  音量可以透過按I跟O調整，I變大聲，O變小聲。

5. UI :
    *  玩家血量: 右上角的愛心。
    *  大招次數: 右上角的黃色飛彈。
    *  分數:     左上角顯示分數。
    *  遊戲暫停: 愛心下方的pause，點擊後就會暫停。



