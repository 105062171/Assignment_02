


var load_state =  {
    preload: function () {
        game.load.image('bullet', 'assets/pepega_2.png');
        game.load.image('enemyBullet', 'assets/enemy-bullet.png');
        game.load.image('ult', 'assets/ult.png');

        game.load.image('start_button', 'assets/start_btn.png');
        game.load.spritesheet('invader', 'assets/Kappa.png', 40, 50);
        game.load.spritesheet('ship', 'assets/player.png',40,80);
        game.load.spritesheet('LIVES', 'assets/lives.png',40,80);
        game.load.spritesheet('ULTI', 'assets/ult_bullet.png',42,124);
        game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
        //particle
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('pixel1', 'assets/emit.png');

        game.load.image('backgroud', 'assets/backgroud.jpg');

        game.load.audio('explosion', 'assets/explosion.mp3');
        game.load.audio('blaster', 'assets/blaster.mp3');
        game.load.audio('backgound_sound', 'assets/backgound_sound.mp3');
        

        
        
    },
    create: function() {
        // Go to the menu state
        game.state.start('menu');
    } 
};

var player;
var aliens;
var bullets;
var bulletTime = 0;
var bullet_spd = 150;
var bullet_gap = 2000;
var ult_time =0;
var ult_bullets;

var cursors;
var fire_btn;
var ult_btn;
var volumn_up;
var volumn_down;
var explosions;
var starfield;
var ult_ani;
var ULTI;

var score = 0;
var scoreString = '';
var scoreText;
var lives;
var enemyBullet;


var firingTimer = 0;
var stateText;
var livingEnemies = [];
var player_emitter;
var enermy_emitter;

var enermy_spd = 800;
var enermy_spd1 = 1000;
//sound
var explosion;
var blaster;
var backgound_sound;
var pause_label;
var tween;


var play_state = {
    preload: function() {},
    create: function() {
        
        
        //game sound first
        backgound_sound = game.add.audio('backgound_sound');
        backgound_sound.play();
        game.physics.startSystem(Phaser.Physics.ARCADE);
    
        //  The scrolling starfield background
        starfield = game.add.tileSprite(0, 0, 800, 600, 'backgroud');
    
        //  my bullet 
        bullets = game.add.group();
        bullets.enableBody = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;
        bullets.createMultiple(30, 'bullet');
        bullets.setAll('anchor.x', 0.5);
        bullets.setAll('anchor.y', 1);
        bullets.setAll('outOfBoundsKill', true);
        bullets.setAll('checkWorldBounds', true);
        // ult_bullet
        ult_bullets = game.add.group();
        ult_bullets.enableBody = true;
        ult_bullets.physicsBodyType = Phaser.Physics.ARCADE;
        ult_bullets.createMultiple(3, 'ult');
        ult_bullets.setAll('anchor.x', 0.5);
        ult_bullets.setAll('anchor.y', 1);
        ult_bullets.setAll('outOfBoundsKill', true);
        ult_bullets.setAll('checkWorldBounds', true);

        
        // my ship
        player = game.add.sprite(400, 500, 'ship');
        player.animations.add( 'right', [2], 8,true);
        player.animations.add(  'left', [1], 8, true);
        player.anchor.setTo(0.5, 0.5);
        game.physics.enable(player, Phaser.Physics.ARCADE);
    
        // The enemy's bullets
        enemyBullets = game.add.group();
        enemyBullets.enableBody = true;
        enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        enemyBullets.createMultiple(30, 'enemyBullet');
        enemyBullets.setAll('anchor.x', 0.5);
        enemyBullets.setAll('anchor.y', 1);
        enemyBullets.setAll('outOfBoundsKill', true);
        enemyBullets.setAll('checkWorldBounds', true);
    
        //  enemy Rainbow Kappa!
        aliens = game.add.group();
        aliens.enableBody = true;
        aliens.physicsBodyType = Phaser.Physics.ARCADE;
        
        aliens1 = game.add.group();
        aliens1.enableBody = true;
        aliens1.physicsBodyType = Phaser.Physics.ARCADE;
        //audio
        explosion = game.add.audio('explosion');
        blaster = game.add.audio('blaster');
        backgound_sound.volume = 0.5;
        blaster.volume = 0.5;
        //particle 
        player_emitter = game.add.emitter(0, 0, 15);
        player_emitter.makeParticles('pixel');
        player_emitter.setYSpeed(-150, 150);
        player_emitter.setXSpeed(-150, 150);
        player_emitter.setScale(2, 0, 2, 0, 800);
        player_emitter.gravity = 0;


         //particle  enermy
         enermy_emitter = game.add.emitter(0, 0, 15);
         enermy_emitter.makeParticles('bullet');
         enermy_emitter.setYSpeed(-250, 250);
         enermy_emitter.setXSpeed(-250, 250);
         enermy_emitter.setScale(2, 0, 2, 0, 800);
         enermy_emitter.gravity = 0;

        createAliens();
        // pause
        pause_label = game.add.text(700, 80, 'Pause', { font: '30px Arial', fill: '#fff' });
        pause_label.inputEnabled = true;
        pause_label.events.onInputUp.add(function () {
            // When the paus button is pressed, we pause the game
            game.paused = true;
        });
        game.input.onDown.add(unpause, self);

        //  The score
        scoreString = 'Score : ';
        scoreText = game.add.text(10, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });
    
        //  Lives
        lives = game.add.group();
    
        for (var i = 0; i < 3; i++) 
        {
            var ship = lives.create(game.world.width - 100 + (30 * i), 60, 'LIVES');
            ship.anchor.setTo(0.5, 0.5);
        }
        
        // ULTI
        ULTI = game.add.group();

        for (var i = 0; i < 3; i++) 
        {
            var aa = ULTI.create(game.world.width - 100 + (30 * i),15, 'ULTI');
            aa.anchor.setTo(0.5, 0.5);
        }

        //  Text
        stateText = game.add.text(game.world.centerX, game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
        stateText.anchor.setTo(0.5, 0.5);
        stateText.visible = false;
        


        
    
        //  An explosion pool
        explosions = game.add.group();
        explosions.createMultiple(30, 'kaboom');
        explosions.forEach(setupInvader, this);
    
        //  keyboard control
        cursors     = game.input.keyboard.createCursorKeys();
        volumn_up   = game.input.keyboard.addKey(Phaser.Keyboard.I);
        volumn_down = game.input.keyboard.addKey(Phaser.Keyboard.O);
        fire_btn    = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        ult_btn     = game.input.keyboard.addKey(Phaser.Keyboard.Z);
    },
    update: function() {

        //  Scroll the background
        starfield.tilePosition.y += 2;
    
        if (player.alive)
        {
            //  Reset the player, then check for movement keys
            player.body.velocity.setTo(0, 0);
    
            if (cursors.left.isDown)
            {
                if(player.body.x > 0) player.body.velocity.x = -200;
                player.animations.play('left');
            }
            else if (cursors.right.isDown)
            {
                if(player.body.x < 760) player.body.velocity.x = 200;
                player.animations.play('right');
            }
            else if (cursors.up.isDown)
            {
                if(player.body.y > 0) player.body.velocity.y = -200;
                player.animations.stop();
            }
            else if (cursors.down.isDown)
            {
                if(player.body.y < 520) player.body.velocity.y = 200;
                player.animations.stop();
            }
            else{
                player.body.velocity.x = 0;
                player.animations.stop();
                player.frame = 0;
            }
            if (ult_btn.isDown)
            {
                console.log("FIRE ULT!");
                fire_ult();
            }

            //  Fire
            if (fire_btn.isDown)
            {
                blaster.play();
                fireBullet();
            }
            
            if( volumn_up.isDown ){
                console.log("volumn_up");
                console.log(backgound_sound.volume);
                if(backgound_sound.volume < 1) backgound_sound.volume += 0.1;
                if(blaster.volume < 1)   blaster.volume += 0.01;
            }else if( volumn_down.isDown )
            {
                console.log("volumn_down");
                console.log(backgound_sound.volume);
                if(backgound_sound.volume > 0)   backgound_sound.volume -= 0.1;
                if(blaster.volume > 0)   blaster.volume -= 0.01;
            }

            if (game.time.now > firingTimer)
            {
                enemyFires();
            }
    
            //  Run collision
            game.physics.arcade.overlap(bullets, aliens, collisionHandler, null, this);
            game.physics.arcade.overlap(bullets, aliens1, collisionHandler, null, this);
            game.physics.arcade.overlap(ult_bullets, aliens, ULT_collisionHandler, null, this);
            game.physics.arcade.overlap(ult_bullets, aliens1, ULT_collisionHandler, null, this);
            game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);
        }
    
    }

};




function createAliens () {
    //group0
    for (var y = 0; y < 2; y++)
    {
        for (var x = 0; x < 10; x++)
        {
            var X = game.rnd.integerInRange(0, 10);
            var Y =game.rnd.integerInRange(0, 2);
            var alien = aliens.create(X * 48, Y * 50, 'invader');
            alien.anchor.setTo(0.5, 0.5);
            alien.animations.add('fly', [ 0, 1, 2], 20, true);
            alien.play('fly');
            alien.body.moves = false;
        }
    }
    aliens.x = 100;
    aliens.y = 50;
    //moving
    tween = game.add.tween(aliens).to( { x: 200,y : 200}, enermy_spd1, Phaser.Easing.Linear.None, true, 0, 1000, true);
    tween.onLoop.add(descend, this);
    //group1
    for (var y = 2; y < 4; y++)
    {
        for (var x = 0; x < 10; x++)
        {
            var X = game.rnd.integerInRange(0, 10);
            var Y =game.rnd.integerInRange(2, 4);
            var alien1 = aliens1.create(X * 48, Y * 50, 'invader');
            alien1.anchor.setTo(0.5, 0.5);
            alien1.animations.add('fly', [ 0, 1, 2], 20, true);
            alien1.play('fly');
            alien1.body.moves = false;
        }
    }
    aliens1.x = 300;
    aliens1.y = 200;
    //moving
    tween = game.add.tween(aliens1).to( { x: 100,y : 100}, enermy_spd, Phaser.Easing.Linear.None, true, 0, 1000, true);
    tween.onLoop.add(descend, this);
}

function setupInvader (invader) {

    invader.anchor.x = 0.5;
    invader.anchor.y = 0.5;
    invader.animations.add('kaboom');

}

function descend() {

    aliens.y += 10;

}

function ULT_collisionHandler (bullet, alien) {
    
    player_emitter.x = alien.body.x;
    player_emitter.y = alien.body.y;
    player_emitter.start(true, 800, null, 15);
    
    //  When a bullet hits an alien we kill them both
    
    alien.kill();

    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(alien.body.x, alien.body.y);
    explosion.play('kaboom', 30, false, true);
    //explosion sound
    explosion.play();

    if (aliens.countLiving() + aliens1.countLiving() == 0)
    {
        score += 1000;
        scoreText.text = scoreString + score;

        enemyBullets.callAll('kill',this);
        stateText.text = " You Won, \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }

}


function collisionHandler (bullet, alien) {
    
    player_emitter.x = alien.body.x;
    player_emitter.y = alien.body.y;
    player_emitter.start(true, 800, null, 15);
    
    //  When a bullet hits an alien we kill them both
    
    alien.kill();
    bullet.kill();
    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(alien.body.x, alien.body.y);
    explosion.play('kaboom', 30, false, true);
    //explosion sound
    explosion.play();

    if (aliens.countLiving() + aliens1.countLiving() == 0)
    {
        score += 1000;
        scoreText.text = scoreString + score;

        enemyBullets.callAll('kill',this);
        stateText.text = " You Won, \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }

}

function enemyHitsPlayer (player,bullet) {

    bullet.kill();

    enermy_emitter.x = player.body.x;
    enermy_emitter.y = player.body.y;
    enermy_emitter.start(true, 800, null, 15);

    live = lives.getFirstAlive();

    if (live)
    {
        live.kill();
    }

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(player.body.x, player.body.y);
    explosion.play('kaboom', 30, false, true);
    //explosion audio
    explosion.play();
    // When the player dies
    if (lives.countLiving() < 1)
    {
        player.kill();
        enemyBullets.callAll('kill');

        stateText.text=" GAME OVER \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }

}

function enemyFires () {

    //  Grab the first bullet we can from the pool
    enemyBullet = enemyBullets.getFirstExists(false);

    livingEnemies.length=0;

    aliens.forEachAlive(function(alien){

        // put every living enemy in an array
        livingEnemies.push(alien);
    });
    aliens1.forEachAlive(function(alien1){

        // put every living enemy in an array
        livingEnemies.push(alien1);
    });

    

    if (enemyBullet && livingEnemies.length > 0)
    {
        
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);
        var shooter = livingEnemies[random];

        if(livingEnemies.length < 5)   
        {

            enermy_spd =  1300;
            enermy_spd1 = 1500;
            bullet_spd = 600;
            bullet_gap = 1500;
        }
        else if( livingEnemies.length  <  15)   
        {
            enermy_spd =  1100;
            enermy_spd1 = 1200;
            bullet_spd = 300;
            bullet_gap = 1000;
        }

        enemyBullet.reset(shooter.body.x, shooter.body.y);
        game.physics.arcade.moveToObject(enemyBullet,player,bullet_spd);
        firingTimer = game.time.now + bullet_gap;
    }


}
function fire_ult () {

    //  To avoid them being allowed to fire too fast we set a time limit
    ultiiii = ULTI.getFirstAlive();
    if(ultiiii){
        if (game.time.now > ult_time)
        {
            //  Grab the first bullet we can from the pool
            ult_bullet = ult_bullets.getFirstExists(false);

            if (ult_bullet)
            {
                //  And fire it
                ult_bullet.reset(player.x, player.y + 8);
                ult_bullet.body.velocity.y = -400;
                ult_time = game.time.now + 200;
                ultiiii.kill();
            }
            
        }
        
    }
}


function fireBullet () {

    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime)
    {
        //  Grab the first bullet we can from the pool
        bullet = bullets.getFirstExists(false);

        if (bullet)
        {
            //  And fire it
            bullet.reset(player.x, player.y + 8);
            bullet.body.velocity.y = -400;
            bulletTime = game.time.now + 200;
        }
    }

}

function resetBullet (bullet) {

    //  Called if the bullet goes out of the screen
    bullet.kill();
}

function restart () {

    //  A new level starts
    
    //resets the life count
    lives.callAll('revive');
    ULTI.callAll('revive');
    //  And brings the aliens back from the dead :)
    aliens.removeAll();
    aliens1.removeAll();
    createAliens();

    //revives the player
    player.revive();
    //hides the text
    stateText.visible = false;
    bullet_spd = 150;
    bullet_gap = 2000;
    score = 0;
    scoreText.text = scoreString + score;
}

var menu_state = {
    load: function() {
        game.stage.backgroundColor = '#3498db';
    },
    create: function() {
        var nameLabel = game.add.text(game.width/2, 80, 'STARWAR',
        { font: '50px Arial', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);
        var startLabel = game.add.text(game.width/2, game.height-80,
            'press to start', { font: '25px Arial', fill: '#ffffff' });
            startLabel.anchor.setTo(0.5, 0.5);
            var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
            upKey.onDown.add(this.start, this);

        button = game.add.button(game.world.centerX - 150, 380, 'start_button', this.start);
    },
    start: function() {
        // Start the actual game
        game.state.start('play');
    },
};
function unpause(event){

    game.paused = false;
    
    
};



var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas' );
game.state.add('load', load_state);
game.state.add('menu', menu_state);
game.state.add('play', play_state);
game.state.start('load');